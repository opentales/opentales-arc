@startuml
!include diagrams/style.iuml

left to right direction

class Repository {
    UUID: String
    name: String
    tales: Tale[]
    ' ------
    startTale(initialData: DataSet)
}

class Tale {
    name: String
    root: Branch
    branches: Branch[]
    ' ------
    init(initialData: DataSet)
    branch()
    branch(branchFrom: Branch)
    branch(branchFrom: Version)
}

class Branch {
    name: String
    base: Branch
    root: Version
    head: Version
    ' ------
    put(data: DataSet)
    mergeBack()
    mergeTo(branch: Branch)
}

class Version {
    UVN: String
    last: Version
    next: Version
    data: DataSet
    ' ------
    init(data: DataSet)
    calcUVN()
}

class DataSet {
    data: DataPoint[]
    ' ------
    compare(to: DataSet)
}

class DataPoint<T> {
    isRef: bool
    ref: DataPoint
    payload: T
    ' ------
    compare(to: DataPoint)
}

Repository "1" *-- "0..*" Tale
Tale "1" *-- "1..*" Branch
Branch "1" -- "2" Version
Version "1" --> "0..2" Version
Version "1" *-- "1" DataSet
DataSet "1" *-- "1--*" DataPoint
DataPoint --> "0..1" DataPoint

@enduml