# arc-openTales


[![pipeline status](https://gitlab.com/opentales/opentales-arc/badges/master/pipeline.svg)](https://gitlab.com/opentales/opentales-arc/commits/master)

This project holds the architecural documentation of the openTails project.
It uses the [arc42 template](https://github.com/arc42/arc42-template) and is written as [asciidoc](http://asciidoc.org/).

A compiled HTML version of the current master is available as [GitLab Page](https://opentales.gitlab.io/opentales-arc/)